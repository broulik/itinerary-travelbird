/*
    Copyright (C) 2017 by Kai Uwe Broulik <kde@privat.broulik.de>
    Copyright (C) 2017 by David Edmundson <davidedmundson@kde.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/

#include <QApplication>
#include <QDebug>

#include <KCrash>

#include "connection.h"
#include "pluginmanager.h"
#include "abstractbrowserplugin.h"

#include "settings.h"
#include "itineraryplugin.h"

void msgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(type);
    Q_UNUSED(context);

    QJsonObject data;
    data[QStringLiteral("subsystem")] = QStringLiteral("debug");
    switch(type) {
        case QtDebugMsg:
        case QtInfoMsg:
            data[QStringLiteral("action")] = QStringLiteral("debug");
            break;
        default:
            data[QStringLiteral("action")] = QStringLiteral("warning");
    }
    data[QStringLiteral("payload")] = QJsonObject({{QStringLiteral("message"), msg}});

    Connection::self()->sendData(data);
}

int main(int argc, char *argv[])
{
    // otherwise when logging out, session manager will ask the host to quit
    // (it's a "regular X app" after all) and then the browser will complain
    qunsetenv("SESSION_MANAGER");

    // TODO Can probably be QGuiApplication
    QApplication a(argc, argv);
    // otherwise will close when download job finishes
    a.setQuitOnLastWindowClosed(false);
    // TODO do we need to set an application name to "pose" as Thunderbird?

    qInstallMessageHandler(msgHandler);

    KCrash::initialize();

    PluginManager::self().addPlugin(&Settings::self());
    PluginManager::self().addPlugin(new ItineraryPlugin(&a));

    // TODO make this prettier, also prevent unloading them at any cost
    PluginManager::self().loadPlugin(&Settings::self());
    PluginManager::self().loadPlugin(PluginManager::self().pluginForSubsystem(QStringLiteral("itinerary")));

    // FIXME Send some form of lifesign to the extension
    qDebug() << "hola";

    return a.exec();
}
